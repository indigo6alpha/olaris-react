import styled from 'styled-components';

const ResumeOption = styled.button`
    background: none;
    width: 100%;
    border: none;
    text-align: left;
    color: #ffffff60;
    line-height: 4rem;
    font-size: 1.6rem;
    transition: 0.2s all;

    &:hover {
        color: #fff;
    }
`;

export default ResumeOption;
